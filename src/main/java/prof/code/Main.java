package prof.code;

import prof.code.model.SubDigitsDto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> numbers = new ArrayList<>();
        numbers.add("219888888888888888888900130000000000");
        numbers.add("229888888888888888888900130000000000");
        numbers.add("219888888888888888888900330000000000");
        numbers.add("219888888888888888888900430000000000");
        numbers.add("219888888888888888888900530000000000");
        numbers.add("219888888888888888888900630000000000");
        numbers.add("219888888888888888888900730000000000");
        numbers.add("229888888888888888888900830000000000");
        numbers.add("229888888888888888888900930000000000");
        numbers.add("229888888888888888888901030000000000");
        numbers.add("229888888888888888888901130000000000");
        numbers.add("229888888888888888888901230000000000");
        numbers.add("239888888888888888888901330000000000");
        numbers.add("239888888888888888888901430000000000");
        numbers.add("239888888888888888888901530000000000");
        numbers.add("239888888888888888888901630000000000");
        numbers.add("239888888888888888888901730000000000");
        List<SubDigitsDto> subDigitsDtoList = f(numbers);
        subDigitsDtoList.stream().forEach(s -> System.out.printf("%s %s %s\n", s.getYear(), s.getIku(),s.getSubDigits()));
    }

    public static List<SubDigitsDto> f(List<String> numbers) {
        List<SubDigitsDto> subDigitsDtoList = new ArrayList<SubDigitsDto>();
        numbers.forEach(n -> {
            SubDigitsDto subDigitsDto = subDigitsDtoList.stream().filter(s -> s.getYear().contentEquals(n.substring(0, 2))).findFirst().orElse(null);
            if (subDigitsDto == null) {
                subDigitsDto = new SubDigitsDto();
                subDigitsDto.setYear(n.substring(0, 2));
                subDigitsDto.setIku(n.substring(3, 22));
                subDigitsDto.setSubDigits(new HashSet<String>());
                subDigitsDtoList.add(subDigitsDto);
            }
            subDigitsDto.getSubDigits().add(n.substring(23, 26));
        });
        return subDigitsDtoList;
    }
}