package prof.code.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class SubDigitsDto {
    private String year;
    private String iku;
    private Set<String> subDigits;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getIku() {
        return iku;
    }

    public void setIku(String iku) {
        this.iku = iku;
    }

    public Set<String> getSubDigits() {
        return subDigits;
    }

    public void setSubDigits(Set<String> subDigits) {
        this.subDigits = subDigits;
    }
}
